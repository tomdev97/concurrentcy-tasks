## Testing on Node versions:
``bash
  8.12.0 - 10.17.0 - 12.13.0
``

## Running the app

``bash
$ node main.js
``