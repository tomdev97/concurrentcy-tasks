'use strict';
const totalTasks = 10;

const EventEmitter = require('events');
/**
* Used for executing tasks concurrently
*
* enqueue function
* - Return a promise which will be executed
*
* @param {integer} numConcurrentTasks Number of concurrent tasks
* @param {function} getTask Function will be called to enqueue task, will return any available task
*
* @return {object}
*/
const run = async (numConcurrentTasks, getTask, totalTasks) => {
  // TO DO: Implement this function to process tasks concurrently
  // For example, if there are 10 tasks in total and there should be 3 concurrently tasks:
  // - At the first step: the task 1, 2, 3 must start immediately.
  // - If any task is done, the next task musts start immediately.
  // - After the last task starts, there must be no more queueing up.

  const processingEmitter = new EventEmitter();

  const waitForAllDone = () => new Promise( (resolve, reject) => {
    processingEmitter.once('allDone', function(e) {
      resolve(); // done
    });
  });

  if (numConcurrentTasks < 0) {
    return;
  }

  let numberOfDoneTasks = 0;

  processingEmitter.on('oneTaskDone', function() {
    numberOfDoneTasks++;

    const task = getTask();

    if (!task && numberOfDoneTasks < totalTasks) {
      return;
    }

    if (numberOfDoneTasks === totalTasks) {
      return processingEmitter.emit('allDone');
    }

    task()
      .then(() => processingEmitter.emit('oneTaskDone'));
  });

  try {
    const initalTasks = [];
    for (let i = 1; i <= numConcurrentTasks; i++) {
      initalTasks.push(
        getTask()()
          .then(() => processingEmitter.emit('oneTaskDone')),
      );
    }
  
    await Promise.all(initalTasks);
  
    await waitForAllDone();
  } catch (error) {
    Promise.reject(error);
  }
};

/**
* This function is used to stimulate task processing time
*/
const waitFor = ms => {
  return new Promise(resolve => {
    setTimeout(() => resolve(), ms);
  });
};


/**
* Main
*/
const main = async () => {
  const startedAt = Date.now();

  // Define 10 tasks, task i_th would take i*2 seconds to finish
  const tasks = [];
  for (let i = 1; i <= 10; i++) {
    const task = async () => {
      console.log(`Task ${i} started, done in ${i*2}s`);
      await waitFor(i * 2000);
      console.log(`Task ${i} DONE!`);
    };
    tasks.push(task);
  }

  console.log('Processing 3 tasks concurrently');
  // Run 3 tasks concurrently
  await run(3, () => {
    // Shift one task from the list, this task should be queued up for processing
    const task = tasks.shift();
    if (!task) return;

    // return the task for queueing
    return task;
  }, totalTasks);
  console.log(`DONE after ${Date.now() - startedAt}ms`)
};
main().catch(e => console.log(e.stack));